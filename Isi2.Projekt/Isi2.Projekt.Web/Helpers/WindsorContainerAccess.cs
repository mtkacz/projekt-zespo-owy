﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Isi2.Projekt.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace Isi2.Projekt.Web.Helpers
{
    public static class WindsorContainerAccess
    {
        public static WindsorContainer GetContainer()
        {
            var container = new WindsorContainer();
            container.Kernel.AddFacility<WcfFacility>();
            container.Register(Component.For<IService>()
                                   .AsWcfClient(new DefaultClientModel
                                   {
                                       Endpoint = WcfEndpoint.BoundTo(new BasicHttpBinding())
                                           .At("http://localhost:51512/Service.svc")
                                   }));
            return container;
        }
    }
}